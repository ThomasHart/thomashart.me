/* Author: Thomas Hart II / Zen.0 Web Consulting

*/

function cufon_replace() {
	Cufon.replace('h1');
	Cufon.replace('h2');
	Cufon.replace('h3');
	Cufon.replace('.portfolio-heading');
	Cufon.replace('.portfolio-title');
	Cufon.replace('.slider-heading');
	Cufon.replace('.slider-title');
}

function nivo_slide_change_before() {
	$('.home-slide-text').fadeOut(500);
}
function nivo_slide_change_after() {
	//alert("SLIDENUM: " + $('.nivo-slider').data('nivo:vars').currentSlide);
	var target_div = "#home_slider_text #slide" + $('.nivo-slider').data('nivo:vars').currentSlide;
	$(target_div).fadeIn(500);
}

function update_title_div (tab_content, under_tab_content, page_name) {
	$('#tab-content').animate({
		height: 0,
		
		paddingBottom:0
	}, 800, function() {
		// Animation complete.
		$('#tab-content').html(tab_content);
		Cufon.replace('h1');
		var element_height = $('#tab-content').css({height:'100%'}).height();
		
		$('#tab-content').css({height:0}).animate({height:element_height,paddingBottom:30},800, function() {
			
			if(page_name == 'home') {
				$('.nivo-slider').nivoSlider({
					directionNavHide:false, // Only show on hover
					controlNav:false, // 1,2,3... navigation
					manualAdvance:true, // Force manual transitions
					captionOpacity:0.8, // Universal caption opacity
					prevText: '&nbsp;', // Prev directionNav text
					nextText: '&nbsp;', // Next directionNav text
					beforeChange: function(){
						nivo_slide_change_before();
					}, // Triggers before a slide transition
					afterChange: function(){
						nivo_slide_change_after();
					} // Triggers after a slide transition
				
				});
			}
			if(page_name == 'contact') {
				setup_contact_form();
			}
		
		});
	});
	
	$('#under-tab-content').fadeOut(800, function() {
		$('#under-tab-content').html(under_tab_content);
		cufon_replace();
		
		$('.contact_tab_link').click(function() {
		$.ajax({
			url: 'contact.html',
			dataType: 'html',
			success: function(data) {
				$('#tabs .tab').removeClass('active');
				$('#tabs #contact_tab .tab').addClass('active');
				
				var tab_content = $(data).find('#tab-content').html();
				var under_tab_content = $(data).find('#under-tab-content').html();
				
				update_title_div(tab_content, under_tab_content, 'contact');
			}
		});
		
		return false;
	});
		$('#under-tab-content').fadeIn(800);
	});
	
	
}

function setup_contact_form () {
	$('form.contactform input[type=submit]').click(function(){ submit_contact_form(); return false;});
	$('form.contactform').ready(function() {
		$('#message').focus(function() {
			if($(this).val() == "Enter Your Message Here") {
				$(this).val("");
			}
		});
	});
	$('.contactform').validate();
}

function submit_contact_form () {
	$('.contactform').validate();
	
	if($('.contactform').valid()) {
		
		var name = $('input#name').val();
		var email = $('input#email').val();
		var website = $('input#website').val();
		var message = $('textarea#message').val();
		
		var dataString = 'name='+ name + '&email=' + email + '&website=' + website + '&message=' + message;  
		
		$.ajax({  
		  type: "POST",  
		  url: "contact_submit.php",
		  data: dataString,  
		  success: function() {  
		    $('form.contactform').html("<div id='confirmation_message'></div>");  
		    $('#confirmation_message').html("<h2>Contact Form Submitted!</h2>")  
		    .append("<p>We will be in touch soon.</p>")  
		    .hide()  
		    .fadeIn(1500);  
		  }  
		});  
		return false;  
	}
	
}

$(document).ready(function() {
	
	// Cufon Replacement
	cufon_replace();
	
	// set up tabs
	// first we position them
	var tab_index = 3;
	$('#tabs .tab').each(function () {
		//alert("TAB");
		
		$(this).css('right', 15 * tab_index);//.css('z-index', 100 - tab_index);
		tab_index -= 1;
	});
	
	// then set up the ajax links
	$('#tabs #home_tab').click(function() {
		$.ajax({
			url: 'index.html',
			dataType: 'html',
			success: function(data) {
				$('#tabs .tab').removeClass('active');
				$('#tabs #home_tab .tab').addClass('active');
				
				var tab_content = $(data).find('#tab-content').html();
				var under_tab_content = $(data).find('#under-tab-content').html();
				
				update_title_div(tab_content, under_tab_content, 'home');
				
				
			}
		});
		
		return false;
	});
	
	// Set up Contact form
	setup_contact_form();	
	
	$('#tabs #work_tab').click(function() {
		$.ajax({
			url: 'work.html',
			dataType: 'html',
			success: function(data) {
				$('#tabs .tab').removeClass('active');
				$('#tabs #work_tab .tab').addClass('active');
				
				var tab_content = $(data).find('#tab-content').html();
				var under_tab_content = $(data).find('#under-tab-content').html();
				
				update_title_div(tab_content, under_tab_content, 'work');
				
			}
		});
		
		return false;
	});

	$('#tabs #about_tab').click(function() {
		$.ajax({
			url: 'about.html',
			dataType: 'html',
			success: function(data) {
				$('#tabs .tab').removeClass('active');
				$('#tabs #about_tab .tab').addClass('active');
				
				var tab_content = $(data).find('#tab-content').html();
				var under_tab_content = $(data).find('#under-tab-content').html();
				
				update_title_div(tab_content, under_tab_content, 'about');
				
			}
		});
		
		return false;
	});	
	
	$('#tabs #contact_tab').click(function() {
		$.ajax({
			url: 'contact.html',
			dataType: 'html',
			success: function(data) {
				$('#tabs .tab').removeClass('active');
				$('#tabs #contact_tab .tab').addClass('active');
				
				var tab_content = $(data).find('#tab-content').html();
				var under_tab_content = $(data).find('#under-tab-content').html();
				
				update_title_div(tab_content, under_tab_content, 'contact');
			}
		});
		
		return false;
	});
	
	$('.contact_tab_link').click(function() {
		$.ajax({
			url: 'contact.html',
			dataType: 'html',
			success: function(data) {
				$('#tabs .tab').removeClass('active');
				$('#tabs #contact_tab .tab').addClass('active');
				
				var tab_content = $(data).find('#tab-content').html();
				var under_tab_content = $(data).find('#under-tab-content').html();
				
				update_title_div(tab_content, under_tab_content, 'contact');
			}
		});
		
		return false;
	});
	
	// nivo slider init
	if($('.nivo-slider').length > 0) {
		$('.nivo-slider').nivoSlider({
			directionNavHide:false, // Only show on hover
			controlNav:false, // 1,2,3... navigation
			manualAdvance:false, // Force manual transitions
			captionOpacity:0.8, // Universal caption opacity
			prevText: '&nbsp;', // Prev directionNav text
			nextText: '&nbsp;', // Next directionNav text
			pauseTime: 8000,
			beforeChange: function(){
				nivo_slide_change_before();
			}, // Triggers before a slide transition
			afterChange: function(){
				nivo_slide_change_after();
			} // Triggers after a slide transition
		});
	}
});