// default.js
// main javascript file for ThomasHart.me

$(document).ready(function(){
	
	// Round the corners on section elements
	$('div#header, div#content, div#footer, div#sidebar > div').corner();
	
});
