<?php

	$my_email = "tom@thomashart.me";
	$email_subject = "Contact Form Submitted on ThomasHart.me";
	
	$name = $_POST['name'];
	$email = $_POST['email'];
	$website = $_POST['website'];
	$message = $_POST['message'];
	
	
	$email_message = "Form details below.\n\n";
	
	function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
     
    $email_message .= "Name: ".clean_string($name)."\n";
    $email_message .= "Email: ".clean_string($email)."\n";
	$email_message .= "Website: ".clean_string($website)."\n";
	$email_message .= "Message:\n".clean_string($message)."\n";
	
	// create email headers
	$headers = 'To: '.$my_email."\r\n".
	"From: noreply@thomashart.me\r\n".
	'Reply-To: '.$email_from."\r\n" .
	'X-Mailer: PHP/' . phpversion();
	@mail($email_to, $email_subject, $email_message, $headers);

?>